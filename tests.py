from unittest import TestCase
from bundles import Bundles, Orders


class TestBundles(TestCase):

    def setUp(self):
        bundles = Bundles.from_list([
            ['IMG', 5, 450],
            ['IMG', 10, 800],
            ['FLAC', 3, 427.5],
            ['FLAC', 6, 810],
            ['FLAC', 9, 1147.5],
            ['VID', 3, 570],
            ['VID', 5, 900],
            ['VID', 9, 1530],
        ])

        orders = Orders(bundles)
        orders.add_from_file('orders.txt')

        self.orders = orders

    def test_img_breakdown(self):
        total = self.orders.total_summary('IMG')
        orders = self.orders.get_by_format_code('IMG')
        assert total == '10 IMG $800'
        assert str(orders[0]) == '1 x 10 IMG $800'

    def test_flac_breakdown(self):
        total = self.orders.total_summary('FLAC')
        orders = self.orders.get_by_format_code('FLAC')
        assert total == '15 FLAC $1957.5'
        assert str(orders[0]) == '1 x 9 FLAC $1147.5'
        assert str(orders[1]) == '1 x 6 FLAC $810'

    def test_vid_breakdown(self):
        total = self.orders.total_summary('VID')
        orders = self.orders.get_by_format_code('VID')
        assert total == '13 VID $2370'
        assert str(orders[0]) == '2 x 5 VID $900'
        assert str(orders[1]) == '1 x 3 VID $570'
