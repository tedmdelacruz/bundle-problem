## Bundle Problem

Given a set of bundles and orders of bundles, get a formatted breakdown of the orders 

## Setup

```sh
> pip install -r requirements.txt
```


## Test
```sh
> nosetests
```

### Where to look

The input is in `orders.txt`, solution logic is in `bundles.py`, and the tests are in `tests.py`

### Development Notes
The `bundle.Bundle` and `bundle.Bundles` classes are written to make creation of new bundles easy, keeping extensibility in mind. In the real world, I would implement these in a database (or a NoSQL datastore for fast access) so that operators can easily manage the bundles.

I also applied Dynamic Programming principles in `bundles.Orders.add.pick` to get the bundle quantities of the orders.

### Author

Ted Mathew dela Cruz

Former software engineer of Freelancer.com

Former senior software engineer of YOYO Holdings, Philippines

Former senior software engineer of Galleon.PH

[http://tedmdelacruz.com](http://tedmdelacruz.com)
