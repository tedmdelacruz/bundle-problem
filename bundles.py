from operator import attrgetter


class Bundle:
    valid_format_codes = ['IMG', 'FLAC', 'VID']

    def __init__(self, format_code, quantity, price):
        if format_code not in self.valid_format_codes:
            raise ValueError('Not a valid format code')

        self.format_code = format_code
        self.quantity = quantity
        self.price = price

    def __le__(self, other):
        return other >= self.quantity

    def __divmod__(self, other):
        return divmod(other, self.quantity)

    __rdivmod__ = __divmod__

    def __repr__(self):
        return f"Bundle('{self.format_code}', {self.price}, {self.quantity})"

    def __str__(self):
        return f'{self.quantity} {self.format_code} ${self.price}'

    def __mul__(self, other):
        return self.quantity * other

    __rmul__ = __mul__


class Bundles:
    bundles = []

    def __init__(self, bundles):
        self.bundles = bundles

    def get_by_format_code(self, format_code):
        """Returns a list of bundles matching the given format code"""

        return [
            bundle
            for bundle in self.bundles
            if bundle.format_code == format_code
        ]

    @classmethod
    def from_list(cls, bundles):
        return cls([Bundle(*args) for args in bundles])

    def __iter__(self):
        return iter(self.bundles)


class Order:
    """Bundle order

    Keeps track of the bundles selected and the number of bundles ordered
    """

    def __init__(self, bundle: Bundle, quantity):
        self.bundle, self.quantity = bundle, quantity

    def __repr__(self):
        return f"Order('{self.bundle}', {self.quantity})"

    def __str__(self):
        """String representation of the Order

        Ex. 1 x 10 IMG $800
        """
        return f'{self.quantity} x {self.bundle}'


class Orders:

    def __init__(self, bundles: Bundles):
        self.orders = []
        self.bundles = bundles

    def add_from_file(self, file):
        """Adds orders from a text file"""

        with open(file) as f:
            for order in f:
                quantity, format_code = order.rstrip().split(' ')
                self.add(format_code, int(quantity))

    def add(self, format_code, quantity):
        """Adds bundle orders based on the format and quantity provided"""

        match_bundles = self.bundles.get_by_format_code(format_code)

        if not match_bundles:
            return None

        bundles = sorted(
            match_bundles,
            key=attrgetter('quantity'),
            reverse=True
        )

        # Determine the quantities of bundles to add
        chosen_bundles = [0 for i in range(0, len(bundles))]

        def pick(capacity, pick_bundle):
            if 0 in [capacity, len(pick_bundle)]:
                return capacity

            next_bundle = pick_bundle[1:len(pick_bundle)]
            if capacity >= pick_bundle[0]:
                qty_to_pick, remainder = divmod(capacity, pick_bundle[0])

                # Pick this bundle
                chosen_bundles[len(bundles) - len(pick_bundle)] = qty_to_pick

                return pick(remainder, next_bundle)

            return pick(capacity, next_bundle)

        for i in range(0, len(bundles)):
            remainder = pick(quantity, bundles[i:len(bundles)])

            if not remainder:
                break

            chosen_bundles = [0 for i in range(0, len(bundles))]

        # Create the Orders for the chosen bundles
        for i in range(0, len(bundles)):
            qty = chosen_bundles[i]
            if qty > 0:
                self.orders.append(Order(bundles[i], qty))

    def get_by_format_code(self, format_code):
        return [
            order
            for order in self.orders
            if order.bundle.format_code == format_code
        ]

    def total_summary(self, format_code):
        """Provides the total summary of a format code"""
        orders = self.get_by_format_code(format_code)

        total_qty = total_price = 0
        for order in orders:
            total_qty += order.bundle.quantity * order.quantity
            total_price += order.bundle.price * order.quantity

        return f'{total_qty} {format_code} ${total_price}'

    def __iter__(self):
        return iter(self.orders)
